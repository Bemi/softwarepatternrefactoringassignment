import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;

public class AddRecordDialog extends JDialog implements ActionListener {
	private JTextField idField, ppsField, surnameField, firstNameField, salaryField;
	private JComboBox<String> genderCombo, departmentCombo, fullTimeCombo;
	private JButton save, cancel;
	private EmployeeDetails parent;

	public AddRecordDialog(EmployeeDetails parent) {
		setTitle("Add Record");
		setModal(true);
		this.parent = parent;
		this.parent.setEnabled(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JScrollPane scrollPane = new JScrollPane(dialogPane());
		setContentPane(scrollPane);

		getRootPane().setDefaultButton(save);

		setSize(500, 370);
		setLocation(350, 250);
		setVisible(true);
	}

	private Container dialogPane() {
		JPanel empDetails, buttonPanel;
		empDetails = new JPanel(new MigLayout());
		buttonPanel = new JPanel();
		JTextField field;
		String labelConstraints = "growx, pushx";
		String textFieldConstraints = "growx, pushx, wrap";
		empDetails.setBorder(BorderFactory.createTitledBorder("Employee Details"));

		empDetails.add(new JLabel("ID:"), labelConstraints);
		empDetails.add(idField = new JTextField(20), textFieldConstraints);
		idField.setEditable(false);
		

		empDetails.add(new JLabel("PPS Number:"), labelConstraints);
		empDetails.add(ppsField = new JTextField(20), textFieldConstraints);

		empDetails.add(new JLabel("Surname:"), labelConstraints);
		empDetails.add(surnameField = new JTextField(20), textFieldConstraints);

		empDetails.add(new JLabel("First Name:"), labelConstraints);
		empDetails.add(firstNameField = new JTextField(20), textFieldConstraints);

		empDetails.add(new JLabel("Gender:"), labelConstraints);
		empDetails.add(genderCombo = new JComboBox<>(this.parent.gender), textFieldConstraints);

		empDetails.add(new JLabel("Department:"), labelConstraints);
		empDetails.add(departmentCombo = new JComboBox<>(this.parent.department), textFieldConstraints);

		empDetails.add(new JLabel("Salary:"), labelConstraints);
		empDetails.add(salaryField = new JTextField(20), textFieldConstraints);

		empDetails.add(new JLabel("Full Time:"), labelConstraints);
		empDetails.add(fullTimeCombo = new JComboBox<>(this.parent.fullTime), textFieldConstraints);

		buttonPanel.add(save = new JButton("Save"));
		save.addActionListener(this);
		save.requestFocus();
		buttonPanel.add(cancel = new JButton("Cancel"));
		cancel.addActionListener(this);

		empDetails.add(buttonPanel, "span 2,growx, pushx,wrap");
		
		for (int i = 0; i < empDetails.getComponentCount(); i++) {
			empDetails.getComponent(i).setFont(this.parent.font1);
			if (empDetails.getComponent(i) instanceof JComboBox) {
				empDetails.getComponent(i).setBackground(Color.WHITE);
			}
			else if(empDetails.getComponent(i) instanceof JTextField){
				field = (JTextField) empDetails.getComponent(i);
				if(field == ppsField)
					field.setDocument(new JTextFieldLimit(9));
				else
				field.setDocument(new JTextFieldLimit(20));
			}
		}
		
		idField.setText(Integer.toString(this.parent.getNextFreeId()));
		return empDetails;
	}

	private void addRecord() {
		boolean fullTime = false;
		Employee theEmployee;

		if (((String) fullTimeCombo.getSelectedItem()).equalsIgnoreCase("Yes"))
			fullTime = true;
		
		theEmployee = new Employee(Integer.parseInt(idField.getText()), ppsField.getText().toUpperCase(), surnameField.getText().toUpperCase(),
				firstNameField.getText().toUpperCase(), genderCombo.getSelectedItem().toString().charAt(0),
				departmentCombo.getSelectedItem().toString(), Double.parseDouble(salaryField.getText()), fullTime);
		this.parent.currentEmployee = theEmployee;
		this.parent.addRecord(theEmployee);
		this.parent.displayRecords(theEmployee);
	}

	private boolean checkInput() {
		boolean valid = true;
		Color color = new Color(255, 150, 150);

		if (ppsField.getText().equals("")) {
			ppsField.setBackground(color);
			valid = false;
		}
		if (this.parent.correctPps(this.ppsField.getText().trim(), -1)) {
			ppsField.setBackground(color);
			valid = false;
		}
		if (surnameField.getText().isEmpty()) {
			surnameField.setBackground(color);
			valid = false;
		}
		if (firstNameField.getText().isEmpty()) {
			firstNameField.setBackground(color);
			valid = false;
		}
		if (genderCombo.getSelectedIndex() == 0) {
			genderCombo.setBackground(color);
			valid = false;
		}
		if (departmentCombo.getSelectedIndex() == 0) {
			departmentCombo.setBackground(color);
			valid = false;
		}
		try {
			if (Double.parseDouble(salaryField.getText()) < 0) {
				salaryField.setBackground(color);
				valid = false;
			}
		}catch (NumberFormatException num) {
			salaryField.setBackground(color);
			valid = false;
		}
		if (fullTimeCombo.getSelectedIndex() == 0) {
			fullTimeCombo.setBackground(color);
			valid = false;
		}
		return valid;
	}

	private void setToWhite() {
		ppsField.setBackground(Color.WHITE);
		surnameField.setBackground(Color.WHITE);
		firstNameField.setBackground(Color.WHITE);
		salaryField.setBackground(Color.WHITE);
		genderCombo.setBackground(Color.WHITE);
		departmentCombo.setBackground(Color.WHITE);
		fullTimeCombo.setBackground(Color.WHITE);
	}

	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == save) {
			if (checkInput()) {
				addRecord();
				dispose();
				this.parent.changesMade = true;
			}else {
				JOptionPane.showMessageDialog(null, "Wrong values or format! Please check!");
				setToWhite();
			}
		}
		else if (e.getSource() == cancel)
			dispose();
	}
}