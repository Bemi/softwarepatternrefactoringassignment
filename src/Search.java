import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.*;

public class Search  extends JDialog implements ActionListener{
	private EmployeeDetails parent;
	private JButton search, cancel;
	private JTextField searchField;
	private String filterBy;

	public Search(EmployeeDetails parent, JButton search, JButton cancel, JTextField searchField , String filterBy) {
		this.parent = parent;
		this.search = search;
		this.cancel = cancel;
		this.searchField = searchField;
		this.filterBy = filterBy;
	}

	public Container searchPane() {
		JPanel searchPanel = new JPanel(new GridLayout(3, 1));
		JPanel textPanel = new JPanel();
		JPanel buttonPanel = new JPanel();
		JLabel searchLabel;

		searchPanel.add(new JLabel("Search by "+this.filterBy));

		textPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		textPanel.add(searchLabel = new JLabel("Enter "+this.filterBy+":"));
		searchLabel.setFont(this.parent.font1);
		textPanel.add(searchField = new JTextField(20));
		searchField.setFont(this.parent.font1);
		searchField.setDocument(new JTextFieldLimit(20));

		buttonPanel.add(search = new JButton("Search"));
		search.addActionListener(this);
		search.requestFocus();

		buttonPanel.add(cancel = new JButton("Cancel"));
		cancel.addActionListener(this);

		searchPanel.add(textPanel);
		searchPanel.add(buttonPanel);

		return searchPanel;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == search ) {
			if(this.filterBy.equals("ID")) {
				try {
					Double.parseDouble(searchField.getText());
					this.parent.searchByIdField.setText(searchField.getText());
					this.parent.searchEmployeeById();
					dispose();
				} catch (NumberFormatException num) {
					searchField.setBackground(new Color(255, 150, 150));
					JOptionPane.showMessageDialog(null, "Wrong ID format!");
				}
			}else if(this.filterBy.equals("Surname")){
				this.parent.searchBySurnameField.setText(searchField.getText());
				this.parent.searchEmployeeBySurname();
				dispose();
			}
		}
		else if (e.getSource() == cancel)
			dispose();
	}

}