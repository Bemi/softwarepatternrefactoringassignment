import javax.swing.*;

public class SearchByIdDialog extends JDialog {
	private EmployeeDetails parent;
	private JButton search, cancel;
	private JTextField searchField;

	public SearchByIdDialog(EmployeeDetails parent) {
		setTitle("Search by Id");
		setModal(true);
		this.parent = parent;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Search searchDialog = new Search(parent, search, cancel, searchField, "ID");
		JScrollPane scrollPane = new JScrollPane(searchDialog.searchPane());
		setContentPane(scrollPane);

		getRootPane().setDefaultButton(search);

		setSize(500, 190);
		setLocation(350, 250);
		setVisible(true);
	}
}